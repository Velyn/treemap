/*  INF3105 gr. 030 Été 2019                             *
 *  Structures de données et algorithmes                 *
 *  TP2                                                  *
 *                                                       *
 *  HASY04089702 Yassine Hasnaoui                        *
 *                                                      */

#include <iostream>
#include <regex>
using namespace std;

struct noeud
{
    public:
    //constructeur vide
    noeud(){}
    //constructeur avec argum
    noeud(double clef, double valeur){
        Clef=clef;
        Valeur=valeur;
        ValeurMax=valeur;
        hauteurNoeud=0;
        ptrGauche=nullptr;
        ptrDroit=nullptr;
        ptrParent=nullptr;
    }
    //constructeur de copie
    noeud( const noeud & other )
     {
        Clef=other.Clef;
        Valeur=other.Valeur;
        hauteurNoeud=other.hauteurNoeud;
        ptrGauche=other.ptrGauche;
        ptrDroit=other.ptrDroit;
        ptrParent=other.ptrParent;   
     }; 
     //les pointeurs du noeud

    noeud *ptrGauche;
    noeud *ptrDroit;
    noeud *ptrParent;
    double Clef;
    double Valeur;
    int hauteurNoeud;
    int facteur=0;
    double ValeurMax;
    int UpdateHauteurNoeud(noeud *nDroit,noeud *nGauche){
        int HauteurDroite;
        int HauteurGauche;
        int hauteur;

        if (nDroit==nullptr)
        {
            /* code */
            HauteurDroite=0;
        }else
        {
            /* code */
            HauteurDroite=nDroit->hauteurNoeud;
        }
         if (nGauche==nullptr)
        {
            /* code */
            HauteurGauche=0;
        }else
        {
            /* code */
            HauteurGauche=nGauche->hauteurNoeud;
        }
        if (nDroit==nullptr&&nGauche==nullptr)
        {
            /* code */
            hauteur=0;
        }else{
        hauteur= 1+ max(HauteurDroite,HauteurGauche);
        }
        return hauteur;
        }

    int UpdateFacteurEquilibre(noeud *nDroit,noeud *nGauche){
        int HauteurDroite;
        int HauteurGauche;
        int facteur;

        if (nDroit==nullptr)
        {
            HauteurDroite=0;
        }else
        {
            /* code */
            HauteurDroite=nDroit->hauteurNoeud +1;
        }
         if (nGauche==nullptr)
        {
            /* code */
            HauteurGauche=0;
        }else
        {
            /* code */
            HauteurGauche=nGauche->hauteurNoeud +1;
        }
        facteur=(HauteurGauche)-(HauteurDroite);

        return facteur;
    }
    void Update(){
         int HauteurDroite;
        int HauteurGauche;

        if (ptrDroit==nullptr)
        {
            /* code */
            HauteurDroite=0;
        }else
        {
            /* code */
            HauteurDroite=ptrDroit->hauteurNoeud;
        }
         if (ptrGauche==nullptr)
        {
            /* code */
            HauteurGauche=0;
        }else
        {
            /* code */
            HauteurGauche=ptrGauche->hauteurNoeud;
        }
        if (ptrDroit==nullptr&&ptrGauche==nullptr)
        {
            /* code */
            hauteurNoeud=0;
        }else{
        hauteurNoeud= 1+ max(HauteurDroite,HauteurGauche);
        }


        if (ptrGauche!=nullptr)
        {
            ptrGauche->Update();
        }
            
        if (ptrDroit!=nullptr)
        {
            ptrDroit->Update();
        }
        
    }
    bool equilibre(){
        bool equilibre = true;
        if (abs(facteur)>1)
        {
            /* code */
            equilibre=false;
        }
        return equilibre;
    }
    bool DesiquilibreDroit(){
        bool DesiquilibreDroit=false;
        int hauteurDroit;
        int hauteurGauche;
        if (ptrDroit==nullptr)
        {
            /* code */
            hauteurDroit=0;
        }else
        {
            /* code */
            hauteurDroit=ptrDroit->hauteurNoeud;
        }
        
        if (ptrGauche==nullptr)
        {
            /* code */
            hauteurGauche=0;
        }else
        {
            hauteurGauche=ptrGauche->hauteurNoeud;
        }      
        if (hauteurDroit > hauteurGauche)
        {
            /* code */
            DesiquilibreDroit=true;
        }
        return DesiquilibreDroit;     
    }
    bool DesiquilibreGauche(){
        bool DesiquilibreGauche=false;
        int hauteurDroit;
        int hauteurGauche;
        if (ptrDroit==nullptr)
        {
            /* code */
            hauteurDroit=0;
        }else
        {
            /* code */
            hauteurDroit=ptrDroit->hauteurNoeud;
        }
        
        if (ptrGauche==nullptr)
        {
            /* code */
            hauteurGauche=0;
        }else
        {
            hauteurGauche=ptrGauche->hauteurNoeud;
        }  
        if (hauteurGauche > hauteurDroit)
        {
            /* code */
            DesiquilibreGauche=true;
        }
        return DesiquilibreGauche;
    }

    bool estRacine(){
        bool estracine=false;
        if (ptrParent==nullptr)
        {
            /* code */
            estracine=true;
        }
        return estracine;
        
    }
    double UpdateValeurMax(){
        //si le noeud est une feuille
        if (hauteurNoeud==0 ||(ptrGauche==nullptr && ptrDroit==nullptr))
        {
            /* code */
            ValeurMax=Valeur;
        }else //sinon
        {
            /* code */

               if(ptrGauche==nullptr){ValeurMax=max(ValeurMax,max(0.0,ptrDroit->ValeurMax));}
               else if (ptrDroit==nullptr)
               {ValeurMax=max(ValeurMax,max(ptrGauche->ValeurMax,0.0));}
               else 
               {ValeurMax=max(ValeurMax,max(ptrGauche->ValeurMax,ptrDroit->ValeurMax)); }
                             
        }
        return ValeurMax;
     }
};

//Definition de l'arbre Avl
struct AvlTree
{
    public:
    /* data */
    //Constructeur de larbre
    AvlTree(){
        hauteur= -1; //arbre vide
        noeudTemp=nullptr;
        noeudCourant=nullptr;
    }
    void inserer( double clef, double valeur ){
        //comparaison de la clef avec la racine
        if (hauteur== -1) //Si l'arbre est vide
        {
            /* code */
            racine=new noeud(clef,valeur);
            noeudCourant=racine;
            hauteur++;// hauteur de 0
        }else if(clef< noeudCourant->Clef && noeudCourant->ptrGauche==nullptr)//insertion gauche
        {
            /* code */
            noeudCourant->ptrGauche= new noeud(clef,valeur);
            noeudCourant->ptrGauche->ptrParent=noeudCourant;
            noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->UpdateValeurMax();
            //facteur dequilibre
        }else if (clef> noeudCourant->Clef && noeudCourant->ptrDroit==nullptr)// insertion droite
        {
            /* code */
            noeudCourant->ptrDroit= new noeud(clef,valeur);
            noeudCourant->ptrDroit->ptrParent=noeudCourant;
            noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->UpdateValeurMax();

        }else
        {
            /* code */
            //on change le noeud courant
            // La recursivite se passe ici
            if(clef< noeudCourant->Clef){
                noeudCourant=noeudCourant->ptrGauche;
                inserer(  clef,  valeur );
            }else if(clef> noeudCourant->Clef){
                noeudCourant=noeudCourant->ptrDroit;
                inserer(  clef,  valeur );
            }          
        }

        //si le noeud courant ne pointe pas vers la racine de l'arbre
        if (noeudCourant!=racine)
        {
            /* code */       
            noeudCourant=noeudCourant->ptrParent;
            noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->UpdateValeurMax();
           
           
            if (noeudCourant->equilibre()==false)// si il y a un desiquilibre
            {

                //si le sous arbre du noeud est desiquilibrer a DROITE
                if (noeudCourant->DesiquilibreDroit()) {RotationGauche(false);}
                else if (noeudCourant->DesiquilibreGauche()) {RotationDroite(false);}
     
                if(noeudCourant->ptrDroit!=nullptr){noeudCourant->ptrDroit->UpdateValeurMax();}
                if(noeudCourant->ptrGauche!=nullptr){noeudCourant->ptrGauche->UpdateValeurMax();}
                 noeudCourant->UpdateValeurMax();
             
            }
            hauteur=noeudCourant->hauteurNoeud;//?

            if (noeudCourant->estRacine()){racine=noeudCourant;} //mise a jour du pointeur racines
            else if (noeudCourant->ptrParent->estRacine()){racine=noeudCourant->ptrParent;}
                        

        }else{
        //quand on arrive a la racine
         noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
         noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
         noeudCourant->UpdateValeurMax();
         hauteur=noeudCourant->hauteurNoeud;
        }
     
    }
    double maxima(){return racine->ValeurMax;}
    double maxima( double clef ){
        int compteur=0;
        double Maxima=0;
        //recherche recursive
        noeudCourant=racine;
        if (noeudCourant->Clef == clef )
        {
            /* code */
            Maxima=noeudCourant->ValeurMax;
        }else
        {
            while (noeudCourant->Clef != clef )//attention peut causer des boucle infinie si clef nexiste pas
               {
                    /* code */
                    if (clef<noeudCourant->Clef)
                    {
                        /* code */
                        noeudCourant=noeudCourant->ptrGauche;
                    }else if (clef>noeudCourant->Clef)
                    {
                        /* code */
                        noeudCourant=noeudCourant->ptrDroit;

                    }  
                    compteur++;
                }
                Maxima=noeudCourant->ValeurMax;
        } 
        noeudCourant=racine;// on replace le noeud courant a la racine
        return Maxima;
    }
    vector< pair< double, double > > jusqua (  noeud *n,double x ){
        //retourne la liste des paires contenant la clef et la valeur des nœuds dont la clef est plus petite ou égale à x. Utilisé pour la
        //commande avant.
        noeudCourant=n;
        if (noeudCourant==nullptr)
        {
            /* code */
        }else if(noeudCourant->Clef<=x)
        {
            /* code */
           
            List.push_back(std::make_pair(noeudCourant->Clef,noeudCourant->Valeur));
            if(noeudCourant->ptrGauche!=nullptr){jusqua(noeudCourant->ptrGauche,x);}
            noeud *ptrTemp=noeudCourant;
            if(noeudCourant->ptrParent!=nullptr){noeudCourant=noeudCourant->ptrParent;}
            if(noeudCourant->ptrDroit!=nullptr&&noeudCourant->ptrDroit!=ptrTemp){jusqua(noeudCourant->ptrDroit,x);}
        }else
        {
            /* code */
            if(noeudCourant->ptrGauche!=nullptr){jusqua(noeudCourant->ptrGauche,x);}
        }
       return List;
    }

    double aGauche( noeud *n, double x ){ // doit avoir un seul arg
        // retourne la valeur maximale des valeurs associées aux clefs
        //plus petites ou égales à x. Utilisé pour la commande donne et appartient.
        //exemple x= 115 valeur max = 500
        
        noeudCourant=n;
        //105
        if (n==nullptr)
        {
            /* code */
            ValeurGauche= (-1) * std::numeric_limits<double>::infinity();;
        }else
        if (x<noeudCourant->Clef)
        {
            /* code */
            aGauche(noeudCourant->ptrGauche,x);
        }else
        {
            /* code */
            if(noeudCourant->ptrGauche==nullptr){ValeurGauche=max(  noeudCourant->Valeur  ,max((-1) * std::numeric_limits<double>::infinity(),aGauche(noeudCourant->ptrDroit,x))); }
            else{ValeurGauche=max(  noeudCourant->Valeur  ,max(noeudCourant->ptrGauche->ValeurMax,aGauche(noeudCourant->ptrDroit,x))); }          
        } 
        return ValeurGauche;
    }
    void ReinitialiserNoeudCourant(){noeudCourant=racine;}// on replace le noeud courant a la racine.
    void RotationGauche(bool doubleRotation){  
        //on verifie s'il s'agit d'une rotation double a l'aide du signe du facteur d'equilibre
        if (noeudCourant->ptrDroit->facteur < 0 ||doubleRotation==true) //erreur facteur courant
        {

            //rotation gauche
                noeudCourant->ptrDroit->ptrParent= noeudCourant->ptrParent;
                if (!noeudCourant->estRacine()&&noeudCourant->ptrParent->ptrDroit!=nullptr&&doubleRotation==false){noeudCourant->ptrParent->ptrDroit= noeudCourant->ptrDroit;}
                 
                 
                  if (noeudCourant->ptrDroit->ptrGauche!=nullptr)//si l'enfant a droite possede un enfant gauche
                {
                    /* code */
                    noeudTemp=noeudCourant->ptrDroit->ptrGauche;

                }
                noeudCourant->ptrDroit->ptrGauche=noeudCourant;
                noeudCourant->ptrParent=noeudCourant->ptrDroit;
                if (noeudTemp!=nullptr)
                {
                    /* code */
                    noeudCourant->ptrDroit=noeudTemp;
                    noeudTemp=nullptr;
                    

                }else
                {
                     noeudCourant->ptrDroit=nullptr;

                }
          
                 noeudCourant->hauteurNoeud=0;
                 noeudCourant->facteur=0;
                //mise a jour de l'hauteur et du facteur d'equilibre du noeud courant
              
                 noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
                 noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
                noeudCourant=noeudCourant->ptrParent;
                noeudCourant->ptrParent->ptrGauche=noeudCourant;
                
            
        }else if (noeudCourant->ptrDroit->facteur >0 && doubleRotation== false)
        {
            /* code */

            //rotation droite de l'enfant droit du noeud courant
            noeudCourant=noeudCourant->ptrDroit;
            RotationDroite(true); 
            noeudCourant->ptrParent->ptrDroit=noeudCourant;
            noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            //on revient au noeud courant avant la rotation  droite
            noeudCourant=noeudCourant->ptrParent;
            //rotation gauche du noeud courant
            RotationGauche(true);
            noeudCourant->ptrParent->ptrGauche=noeudCourant;
            noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
            noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);

        }
 
    }
    void RotationDroite(bool doubleRotation){// A verifie
    

     if (noeudCourant->ptrGauche->facteur > 0 ||doubleRotation==true) {
         /* code */

    
                noeudCourant->ptrGauche->ptrParent= noeudCourant->ptrParent;
                
                if (!noeudCourant->estRacine()&&noeudCourant->ptrParent->ptrGauche!=nullptr&&doubleRotation==false&&noeudCourant->equilibre()==true){noeudCourant->ptrParent->ptrGauche= noeudCourant->ptrGauche;}
                if (noeudCourant->ptrGauche->ptrDroit!=nullptr)//si l'enfant a droite possede un enfant gauche
                {
                    /* code */
                    noeudTemp=noeudCourant->ptrGauche->ptrDroit;

                }
               

                //noeud courant : 100
                noeudCourant->ptrGauche->ptrDroit=noeudCourant;
                noeudCourant->ptrParent=noeudCourant->ptrGauche;
         
                if (noeudTemp!=nullptr)
                {
                    /* code */
                    noeudCourant->ptrGauche=noeudTemp;
                    noeudTemp=nullptr;


                }else
                {
                     noeudCourant->ptrGauche=nullptr;
                     

                }
                noeudCourant->hauteurNoeud=0;
                noeudCourant->facteur=0;
                //mise a jour de l'hauteur et du facteur d'equilibre du noeud courant
                 noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
                 noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
                 if(noeudCourant!=racine){noeudCourant=noeudCourant->ptrParent;}
                 if(noeudCourant!=racine){noeudCourant->ptrParent->ptrDroit=noeudCourant;}
                 
     }else if(noeudCourant->ptrGauche->facteur < 0 && doubleRotation== false)
     {
        /* code */

        noeudCourant=noeudCourant->ptrGauche;
        RotationGauche(true);
        noeudCourant->ptrParent->ptrGauche=noeudCourant;
        noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
        noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
        //on revient au noeud courant avant la rotation  droite
        noeudCourant=noeudCourant->ptrParent;
        
        RotationDroite(true);
        if(noeudCourant!=racine){noeudCourant->ptrParent->ptrDroit=noeudCourant;}
        noeudCourant->hauteurNoeud=noeudCourant->UpdateHauteurNoeud(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
        noeudCourant->facteur=noeudCourant->UpdateFacteurEquilibre(noeudCourant->ptrDroit,noeudCourant->ptrGauche);
       

         
     }
    


   
    }
    //racine de l'arbre
    noeud *racine;
    noeud *noeudCourant;
    noeud *noeudTemp;//doit etre placer a null a chaque fin de rotation
    //hauteur de l'arbre
    int hauteur;
    double Maxima;
    double ValeurGauche;
    vector< pair< double, double > > List;
};

int Lecture(string &commande){
    int typeCommande;
    
    //Pattern pour les commande 1 a 5
    regex pattern1 (R"([ ]*\([ ]*\d+[ ]*\.[ ]*\d+[ ]*[,][ ]*\d+\.[ ]*\d+[ ]*\)[ ]*\.[ ]*)");
    regex pattern2 (R"([ ]*m[ ]*a[ ]*x[ ]*\?[ ]*)");
    regex pattern3 (R"([ ]*\d+[ ]*\.[ ]*\d+[ ]*\?[ ]*)");
    regex pattern4 (R"([ ]*donne[ ]*\d+[ ]*\.[ ]*\d+[ ]*\?[ ]*)");
    regex pattern5 (R"([ ]*avant[ ]*\d+[ ]*\.[ ]*\d+[ ]*\?[ ]*)");
    regex pattern6 (R"([ ]*q[ ]*\.[ ]*)");

    //lecture au clavier
    getline(cin,commande);
    //using patterns with regex
    if (regex_match(commande,pattern1))
    {
        /* code */
        typeCommande=1;
    }else if(regex_match(commande,pattern2))
    {
        typeCommande=2;
    }else if(regex_match(commande,pattern3))
    {
        typeCommande=3;
    }else if(regex_match(commande,pattern4))
    {
        typeCommande=4;
    }
    else if(regex_match(commande,pattern5))
    {
        typeCommande=5;
    }else if(regex_match(commande,pattern6))
    {
        typeCommande=6;
    }else //erreur sur cerr
    {
        /* code */
        cerr << "Une erreur commande s'est produite. "<<endl;; 
        typeCommande=0;
    }
    return typeCommande;
    
}

string RemoveSpaces(string input){
    string output;

     for(int i = 0; i < input.length(); i++) {
        if(input[i] == ' ') {
            continue;
        } else {
            output += input[i];
        }
    }
    return output;
}

int main(int argc, char const *argv[])
{
    //instanciation de l'arbre
    AvlTree Tree=AvlTree();
    int Commande;
    string input;

  do{
    Commande = Lecture(input);
    input=RemoveSpaces(input);
    
    if (Commande!=0 &&Commande!=6 )
    {

        if (Commande==1)
        {
            double Clef=atof((input.substr(input.find("(")+1,input.find(","))).c_str()); 
            double Valeur=atof((input.substr(input.find(",")+1,input.find(")"))).c_str());
            Tree.inserer(Clef,Valeur);
            Tree.ReinitialiserNoeudCourant();
            
        }else if (Commande==2)
        {
            cout<<Tree.racine->ValeurMax<<endl;
        }else if (Commande==3)
        {
            double Valeur=atof((input.substr(0,input.find("?"))).c_str());
            if (Valeur <=Tree.aGauche( Tree.racine, Valeur ))//appartien
            {
                cout<<"vrai"<<endl;
            } else
            {
                cout<<"faux"<<endl;
            }      
            Tree.ReinitialiserNoeudCourant();   

        }else if (Commande==4)
        {
            double Clef=atof((input.substr(input.find("e")+1,input.find("?"))).c_str());
            cout<<Tree.aGauche( Tree.racine, Clef )<<endl;
            Tree.ReinitialiserNoeudCourant();
        }else if (Commande==5)//avant
        {
            //Vide la liste pour pouvoir la reconstruire.
            Tree.List.clear(); 
            double Valeur=atof((input.substr(input.find("t")+1,input.find("?"))).c_str());
            Tree.jusqua(Tree.racine,Valeur);
            cout<<"[";
              for(int i = 0; i < Tree.List.size(); i++)
               {
                   if(i!=0){cout<<",";}
                 cout << "("<<Tree.List[i].first << ", " << Tree.List[i].second <<")";
                }
            cout<<"]"<<endl;
            Tree.ReinitialiserNoeudCourant();
        }
    }
  }while(Commande!=6);


    return 0 ;
}